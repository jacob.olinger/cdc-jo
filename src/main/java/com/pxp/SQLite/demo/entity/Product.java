package com.pxp.SQLite.demo.entity;
import javax.persistence.*;
import java.math.BigInteger;
import java.sql.Date;
@Entity
public class Product {

    @Id
    private BigInteger product_id;
    //LaterKey
    private int bot_ref;

    //LaterKey
    private String bot_products;
    //LaterKey
    private String get_products_types;

    //LaterKey
    private String product_created_at_idx;

    //LaterKey
    private int product_id_idx;

    private String shop_domain;

    private String tags;

    private String description;

    private String handle;

    private String status;

    private String vendor;

    private String product_type;

    private String option_1;

    private String option_1_value;

    private String option_2;

    private String option_2_value;

    private String option_3;

    private String option_3_value;

    private String title;

    private Date created_at;

    private Date updated_at;
    private String name;

    private String email;

    public Product() {
    }

    public int getBot_ref() {
        return bot_ref;
    }

    public void setBot_ref(int bot_ref) {
        this.bot_ref = bot_ref;
    }

    public String getBot_products() {
        return bot_products;
    }

    public void setBot_products(String bot_products) {
        this.bot_products = bot_products;
    }

    public String getGet_products_types() {
        return get_products_types;
    }

    public void setGet_products_types(String get_products_types) {
        this.get_products_types = get_products_types;
    }

    public String getProduct_created_at_idx() {
        return product_created_at_idx;
    }

    public void setProduct_created_at_idx(String product_created_at_idx) {
        this.product_created_at_idx = product_created_at_idx;
    }

    public int getProduct_id_idx() {
        return product_id_idx;
    }

    public void setProduct_id_idx(int product_id_idx) {
        this.product_id_idx = product_id_idx;
    }

    public String getShop_domain() {
        return shop_domain;
    }

    public void setShop_domain(String shop_domain) {
        this.shop_domain = shop_domain;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHandle() {
        return handle;
    }

    public void setHandle(String handle) {
        this.handle = handle;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getProduct_type() {
        return product_type;
    }

    public void setProduct_type(String product_type) {
        this.product_type = product_type;
    }

    public String getOption_1() {
        return option_1;
    }

    public void setOption_1(String option_1) {
        this.option_1 = option_1;
    }

    public String getOption_1_value() {
        return option_1_value;
    }

    public void setOption_1_value(String option_1_value) {
        this.option_1_value = option_1_value;
    }

    public String getOption_2() {
        return option_2;
    }

    public void setOption_2(String option_2) {
        this.option_2 = option_2;
    }

    public String getOption_2_value() {
        return option_2_value;
    }

    public void setOption_2_value(String option_2_value) {
        this.option_2_value = option_2_value;
    }

    public String getOption_3() {
        return option_3;
    }

    public void setOption_3(String option_3) {
        this.option_3 = option_3;
    }

    public String getOption_3_value() {
        return option_3_value;
    }

    public void setOption_3_value(String option_3_value) {
        this.option_3_value = option_3_value;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public Date getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Date updated_at) {
        this.updated_at = updated_at;
    }

    public Integer getProduct_id() {
        return Integer.getInteger(String.valueOf(product_id));
    }

    public void setProduct_id(BigInteger product_id) {
        this.product_id = product_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Product{" +
                "product_id=" + product_id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
