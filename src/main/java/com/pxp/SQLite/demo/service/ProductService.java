package com.pxp.SQLite.demo.service;

import com.pxp.SQLite.demo.entity.Product;
import com.pxp.SQLite.demo.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@Service
public class ProductService {
    @Autowired
    private ProductRepository productRepository;

    @Transactional
    public String createProduct(Product product){
        try {
            if (!productRepository.existsByEmail(product.getEmail())){
                product.setProduct_id(BigInteger.valueOf(null == productRepository.findMaxId()? 0 : productRepository.findMaxId() + 1));
                productRepository.save(product);
                return "Product record created successfully.";
            }else {
                return "Product already exists in the database.";
            }
        }catch (Exception e){
            throw e;
        }
    }

    public List<Product> readProduct(){
        return productRepository.findAll();
    }
    public List<Product> getProductById(int ID){
        return productRepository.findAllById(new ArrayList<Integer>(ID));
    }
    public List<Product> getProductByQuery(String query) {
        return productRepository.findByQuery(query);
    }

    @Transactional
    public String updateProduct(Product product){
        if (productRepository.existsByEmail(product.getEmail())){
            try {
                List<Product> products = productRepository.findByEmail(product.getEmail());
                products.stream().forEach(s -> {
                    Product productToBeUpdate = productRepository.findById(s.getProduct_id()).get();
                    productToBeUpdate.setName(product.getName());
                    productToBeUpdate.setEmail(product.getEmail());
                    productRepository.save(productToBeUpdate);
                });
                return "Product record updated.";
            }catch (Exception e){
                throw e;
            }
        }else {
            return "Product does not exists in the database.";
        }
    }

    @Transactional
    public String deleteProduct(Product product){
        if (productRepository.existsByEmail(product.getEmail())){
            try {
                List<Product> products = productRepository.findByEmail(product.getEmail());
                products.stream().forEach(s -> {
                    productRepository.delete(s);
                });
                return "Product record deleted successfully.";
            }catch (Exception e){
                throw e;
            }

        }else {
            return "Product does not exist";
        }
    }


}
