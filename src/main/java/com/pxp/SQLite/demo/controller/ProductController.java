package com.pxp.SQLite.demo.controller;

import com.pxp.SQLite.demo.entity.Product;
import com.pxp.SQLite.demo.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ProductController {

    @Autowired
    private ProductService productService;

    @RequestMapping(value = "info", method = RequestMethod.GET)
    public String info(){
        return "The application is running";
    }

    @RequestMapping(value = "createproduct", method = RequestMethod.POST)
    public String createProducts(@RequestBody Product product){
        return productService.createProduct(product);
    }

    //Key Value Request method
    @RequestMapping(value ="/getProductById/{id}", method = RequestMethod.GET)
        public Product getProductById(@RequestBody @PathVariable("id") int id) {return productService.getProductById(id).get(0);}

    @RequestMapping(value ="/getProductById/{Query}", method = RequestMethod.GET)
    public List<Product> getProductByQuerry(@PathVariable("Query") String Query) {return productService.getProductByQuery(Query);}

    @RequestMapping(value = "readproducts", method = RequestMethod.GET)
    public List<Product> readProducts(){
        return productService.readProduct();
    }

    @RequestMapping(value = "updateproducts", method = RequestMethod.PUT)
    public String updateProducts(@RequestBody Product product){
        return productService.updateProduct(product);
    }

    @RequestMapping(value = "deleteproducts", method = RequestMethod.DELETE)
    public String deleteProducts(@RequestBody Product product){
        return productService.deleteProduct(product);
    }
}
