package com.pxp.SQLite.demo.config;
// Java Program to Illustrate Kafka Configuration
// Importing required classes
import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;

// Annotations
@EnableKafka
@Configuration

// Class
public class KafkaConfig {

    @Bean
    public ConsumerFactory<String, String> consumerFactory()
    {

        // Creating a Map of string-object pairs
        Map<String, Object> config = new HashMap<>();

        // Adding the Configuration
        //localhost configuration
        config.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,
                "127.0.0.1:9092");
        // kafka.topic=Cache_Capture
        config.put(ConsumerConfig.CONFIG_PROVIDERS_CONFIG,"Cache_Capture");
        //        kafka.group.productId=group_id
        config.put(ConsumerConfig.GROUP_ID_CONFIG,
                "group_id");
        //        kafka.session.timeout.ms=20000
        config.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG,"20000");
        //        kafka.enable.auto.commit=true
        config.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG,"TRUE");
        //        kafka.auto.commit.interval.ms=1000
        config.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG,"1000");
        //        kafka.buffer.memory=33554432
        config.put(ConsumerConfig.RECEIVE_BUFFER_CONFIG,"33554432");
        //        kafka.auto.offset.reset=earliest
        config.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG,"earliest");

        //kafka.key.deserializer=org.apache.kafka.common.serialization.StringDeserializer
//        kafka.value.deserializer=org.apache.kafka.common.serialization.StringDeserializer
        config.put(
                ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
                StringDeserializer.class);
        //       kafka.key.serializer=org.apache.kafka.common.serialization.StringSerializer
//        kafka.value.serializer=org.apache.kafka.common.serialization.StringSerializer
        config.put(
                ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
                StringDeserializer.class);

        return new DefaultKafkaConsumerFactory<>(config);
    }

    // Creating a Listener
    public ConcurrentKafkaListenerContainerFactory
    concurrentKafkaListenerContainerFactory()
    {
        ConcurrentKafkaListenerContainerFactory<
                String, String> factory
                = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory());
        return factory;
    }

    @KafkaListener(topics = "Cache_Capture", groupId = "group_id")
    public void listenGroupFoo(String message) {
        System.out.println("Received Message in group foo: " + message);
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, String>
    filterKafkaListenerContainerFactory() {

        ConcurrentKafkaListenerContainerFactory<String, String> factory =
                new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory());
        factory.setRecordFilterStrategy(
                record -> record.value().contains("yo"));
        return factory;
    }
    @KafkaListener(
            topics = "Cache_Capture",
            containerFactory = "filterKafkaListenerContainerFactory")
    public void listenWithFilter(String message) {
        System.out.println("Received Message in filtered listener: " + message);
    }


    @Bean
    public NewTopic Cache_Capture() {
        // kafka.topic=Cache_Capture
        return new NewTopic("baeldung", 1, (short) 1);
    }
}

