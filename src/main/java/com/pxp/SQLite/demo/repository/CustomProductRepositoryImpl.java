package com.pxp.SQLite.demo.repository;

import com.pxp.SQLite.demo.entity.Product;
import org.springframework.stereotype.Repository;
import org.sqlite.SQLiteConfig;

import java.sql.*;
import java.util.List;

@Repository
public interface CustomProductRepositoryImpl {

    public default List<Product> findByQuery(String query){
       Statement statement = getStatement();
       try {
           ResultSet resultSet = statement.executeQuery(query);
           //CONVERT RESULT SET TO LIST OF PRODUCT OBJECTS
           return null;
       } catch (SQLException e) {
           throw new RuntimeException(e);
       }

   }
   //getting an SQLite statement
    private Statement getStatement(){
        Connection con = null;
        try {
            con = getSqliteConnection();
            Statement statement = con.createStatement();
            return statement;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }
//getting a connection to sn SQLite database
    private Connection getSqliteConnection() throws Exception {
       //Setting Configurations for Database Connection
        SQLiteConfig config = new SQLiteConfig();
        //PRAGMA journal_mode = TRUNCATE;       -- Speed
        config.setPragma(SQLiteConfig.Pragma.JOURNAL_MODE, "TRUNCATE");
        //PRAGMA soft_heap_limit = 0;           -- None
        //PRAGMA synchronous = 0;               -- Speed Mode
        config.setPragma(SQLiteConfig.Pragma.SYNCHRONOUS, "0");
        //PRAGMA temp_store = 2;                -- Memory
        config.setPragma(SQLiteConfig.Pragma.TEMP_STORE,"2");
        //PRAGMA cache_size = 610400;           -- ~2.5GB @ 4K/Pg
        config.setPragma(SQLiteConfig.Pragma.CACHE_SIZE,"610400");
//        PRAGMA cell_size_check = 0;           -- No checking
//        PRAGMA ignore_check_constraints = 1;  -- No checking
//        PRAGMA mmap_size = 536870912;         -- 0.5GB (See Compile-Opts)
        config.setPragma(SQLiteConfig.Pragma.MMAP_SIZE,"536870912");
//        PRAGMA threads = 8;                   -- Worker Thread limit

        Connection connection = DriverManager.getConnection("jdbc:sqlite:/Users/apple/Desktop/SQLite.demo/products3.db", config.toProperties());

        return connection;
    }
}
