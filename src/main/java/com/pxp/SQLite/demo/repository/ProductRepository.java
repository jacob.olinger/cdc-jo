package com.pxp.SQLite.demo.repository;

import com.pxp.SQLite.demo.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer>,CustomProductRepositoryImpl{

    public boolean existsByEmail(String email);

    public List<Product> findByEmail(String email);



    @Query("select max(s.id) from Product s")
    public Integer findMaxId();


}
